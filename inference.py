import json
import pandas as pd
import numpy as np
import pystan

data = json.load(open('matches.json', 'r'))

bets = {}
for d in data:
    if 'tours' in d and len(d['tours']):
        match_id = d['tours'][0]['hltvId']

        if match_id == 2310359:
            break

        t1_id = d['tourMatches'][0]['teamId1']
        t2_id = d['tourMatches'][0]['teamId2']

        t1_score = d['tourMatches'][0]['team1Score']
        t2_score = d['tourMatches'][0]['team2Score']

        t1_bets = d['tourBetEvents'][0]['team1Balance']
        t2_bets = d['tourBetEvents'][0]['team2Balance']

        if t1_id == d['playerTeams'][0]['id']:
            t1_hltv_id = d['playerTeams'][0]['hltvId']
            t2_hltv_id = d['playerTeams'][1]['hltvId']
        elif t1_id == d['playerTeams'][1]['id']:
            t1_hltv_id = d['playerTeams'][1]['hltvId']
            t2_hltv_id = d['playerTeams'][0]['hltvId']
        else:
            raise Exception('Chablau')

        if t1_bets == 0 and t2_bets == 0:
            continue

        bets[str(match_id)] = {
            'match_id': str(match_id),
            'team1_id': str(t1_hltv_id),
            'team2_id': str(t2_hltv_id),
            'team1_bets': str(t1_bets),
            'team2_bets': str(t2_bets),
            'team1_score': str(t1_score),
            'team2_score': str(t2_score),
        }

months = {
    'Jan' : 1,
    'Feb' : 2,
    'Mar' : 3,
    'Apr' : 4,
    'May' : 5,
    'Jun' : 6,
    'Jul' : 7,
    'Aug' : 8,
    'Sep' : 9,
    'Oct' : 10,
    'Nov' : 11,
    'Dec' : 12
}
cnt = 0
matches = {d['match_id']: {} for d in data if 'match_id' in d}
for d in data:
    if 'match_id' not in d: continue
    matches[d['match_id']].update(d)
    if 'day' in d:
        matches[d['match_id']]['date'] = d['day']+'/'+str(months[d['month']])+'/'+d['year']

    if d['match_id'] in bets and 'team1_id' in d:
        if d['team1_id'] == bets[d['match_id']]['team1_id'] and d['team2_id'] == bets[d['match_id']]['team2_id']:
            matches[d['match_id']].update(bets[d['match_id']])
            cnt += 1

for match_id in list(matches.keys()):
    if 'team1_rounds' not in matches[match_id] and 'team1_score' not in matches[match_id]:
        del matches[match_id]

for k, v in matches.items():
    v.pop('team1_stats', None)
    v.pop('team2_stats', None)
    v.pop('match_page', None)
    v.pop('bets_page', None)
    v.pop('year', None)
    v.pop('day', None)
    v.pop('month', None)

df = pd.DataFrame.from_dict(matches, orient='index')
df.date = pd.to_datetime(df.date)
df = df[df.date > pd.to_datetime('2015-01-01')]
df = df[df.team1_rounds != df.team2_rounds]

teams = pd.concat([df.team1_id, df.team2_id])
teams = teams.value_counts()
teams = list(teams[teams >= 100].index)

df = df[df.team1_id.isin(teams) & df.team2_id.isin(teams)]

code = '''
data {
    int n;
    int n_teams;
    int team1_id[n];
    int team2_id[n];
    int team1_win[n];
}
parameters {
    real skill[n_teams];
    real<lower=0> alpha;
}
model {
    alpha ~ cauchy(0, 5);
    skill ~ normal(0, 1);
    for(i in 1:n){
        team1_win[i] ~ bernoulli_logit((skill[team1_id[i]] - skill[team2_id[i]])*alpha);
    }
}
'''

teams = list(pd.concat([df.team1_id, df.team2_id]).value_counts().index)
data = {
    'n': len(df),
    'n_teams': len(teams),
    'team1_id': df.team1_id.apply(lambda x: teams.index(x)).values + 1,
    'team2_id': df.team2_id.apply(lambda x: teams.index(x)).values + 1,
    'team1_win': (df.team1_rounds > df.team2_rounds).values.astype('int32')
}

fit = pystan.stan(model_code=code, data=data, iter=10000, n_jobs=4)

print(fit)

fit.plot()

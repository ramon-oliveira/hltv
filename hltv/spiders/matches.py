import scrapy
import json

class MatchesSpider(scrapy.Spider):
    name = 'matches'

    def start_requests(self):
        self.url = 'http://www.hltv.org/?pageid=324&filter=1&clean=1'
        self.formdata = {
            'vod': 'false',
            'intersect': 'false',
            'highlight': 'false',
            'stats': 'true',
            'demo': 'true',
            'offset': '0',
            'daterange': 'All time',
        }
        yield scrapy.FormRequest(url=self.url,
                                 formdata=self.formdata,
                                 callback=self.parse_matches)

    def parse_matches(self, response):
        self.log('Parsing')
        matches = response.xpath('//table/tr')[1:]
        for match in matches:
            date, teams, vod, demo, stats = match.xpath('td')
            t1, t2 = teams.re('\(([0-9]*)-([0-9]*)\)')
            md, year = date.xpath('a/text()').extract_first().split(',')
            year = year.strip()
            month, day = md.split(' ')

            match_page = date.xpath('a/@href').extract_first()
            matchid = match_page.split('/')[2].split('-')[0]

            match_page = response.urljoin(match_page)
            stats_page = 'http://www.hltv.org/?pageid=113&matchid={matchid}&matchStats=1&clean=1'
            stats_page = stats_page.format(matchid=matchid)

            bets_page = 'http://hltv.gainskins.com/api/tours/hid/{matchid}'
            bets_page = bets_page.format(matchid=matchid)

            self.log('Matchid: {0}'.format(matchid))
            self.log('Match page: {0}'.format(match_page))
            self.log('Stats page: {0}'.format(stats_page))

            yield {
                'match_page': match_page,
                'bets_page': bets_page,
                'match_id': matchid,
                'year': year,
                'month': month,
                'day': day,
                'team1_rounds': t1,
                'team2_rounds': t2,
            }

            yield scrapy.Request(bets_page, callback=self.parse_bets)
            yield scrapy.Request(stats_page, callback=self.parse_match_stats)

        if len(matches) == 100:
            self.formdata['offset'] = str(int(self.formdata['offset']) + 100)
            yield scrapy.FormRequest(url=self.url,
                                     formdata=self.formdata,
                                     callback=self.parse_matches)

    def parse_bets(self, response):
        yield json.loads(response.body)

    def parse_match_stats(self, response):
        match_id = response.url.split('matchid=')[1].split('&')[0]
        teams = response.xpath("//a[contains(@href,'teamid')]")
        team1_id = teams[0].re('teamid=([0-9]*)')[0]
        team1_name = teams[0].xpath('text()')[0].extract()
        team2_id = teams[1].re('teamid=([0-9]*)')[0]
        team2_name = teams[1].xpath('text()')[0].extract()

        team1_stats = []
        for p in response.xpath('//div/a/../..')[1:6]:
            player = {
                'player_id': (p.re('playerid=([0-9]*)') + p.re('/player/([0-9]*)'))[0],
                'player_name': p.xpath('div/a').re('>(.*)</a>')[0].replace('<b>', '').replace('</b>', ''),
                'kills': p.xpath('div/text()')[1].extract().split('-')[0],
                'diff': p.xpath('div/text()')[2].extract(),
                'adr': p.xpath('div/text()')[3].extract(),
                'rating': p.xpath('div/text()')[4].extract(),
            }
            team1_stats.append(player)

        team2_stats = []
        for p in response.xpath('//div/a/../..')[7:]:
            player = {
                'player_id': (p.re('playerid=([0-9]*)') + p.re('/player/([0-9]*)'))[0],
                'player_name': p.xpath('div/a').re('>(.*)</a>')[0].replace('<b>', '').replace('</b>', ''),
                'kills': p.xpath('div/text()')[1].extract().split('-')[0],
                'deaths': p.xpath('div/text()')[1].extract().split('-')[1],
                'diff': p.xpath('div/text()')[2].extract(),
                'adr': p.xpath('div/text()')[3].extract(),
                'rating': p.xpath('div/text()')[4].extract(),
            }
            team2_stats.append(player)

        yield {
            'match_id': match_id,
            'team1_id': team1_id,
            'team1_name': team1_name,
            'team1_stats': team1_stats,
            'team2_id': team2_id,
            'team2_name': team2_name,
            'team2_stats': team2_stats,
        }
